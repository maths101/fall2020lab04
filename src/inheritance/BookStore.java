/*Grigor Mihaylov*/
package inheritance;

public class BookStore {
	public static void main(String[] args) {
		Book[] books = new Book[5];
		
		books[0] = new Book("Harry Potter and The Deadly Hollows", "J. K. Rowling");
		books[1] = new ElectronicBook("The Hunger Games", "Suzanne Collins", 4096);
		books[2] = new Book("The Hichhiker's Guide to the Galaxy", "Douglas Adams");
		books[3] = new ElectronicBook("Medecine River", "Thomas King",1024);
		books[4] = new ElectronicBook("Fifty Shades of Grey", "E. L. James",2048);
		
		for (Book b : books) {
			System.out.println(b);
			System.out.println();
		}
	}
}
