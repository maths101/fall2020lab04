/* Grigor Mihaylov */
package inheritance;

public class Book {
	protected String title;
	private String author;
	public String getTitle() {
		return title;
	}
	public String getAuthor() {
		return author;
	}
	public Book(String title, String author) {
		this.title=title;
		this.author = author;
	}
	public String toString() {
	String s;
	s = title + ", by " + author;
	return s;
	}
}
