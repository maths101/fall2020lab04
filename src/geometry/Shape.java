/* Grigor Mihaylov*/
package geometry;

public interface Shape {
	double getArea();
	double getPerimeter();
}
