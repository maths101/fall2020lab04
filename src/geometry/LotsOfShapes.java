/* Grigor Mihaylov*/
package geometry;

public class LotsOfShapes {
	public static void main(String[] args) {
		
	
		Shape[] shapes = new Shape[5];
		shapes[0] = new Circle(5);
		shapes[1]= new Circle(6.5);
		shapes[2] = new Rectangle(4,7);
		shapes[3] = new Rectangle(5.5,4.8);
		shapes[4] = new Square(9);
		
		for (Shape s : shapes) {
			System.out.println("Area: " + s.getArea() + " Perimeter: " + s.getPerimeter());
			System.out.println();
		}
	}
}