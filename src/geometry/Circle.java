/* Grigor Mihaylov*/
package geometry;

public class Circle implements Shape {
	private double radius;
	public Circle(double radius) {
		this.radius=radius;
	}
	public double getRadius() {
		return this.radius;
	}
	
	
	public double getArea() {
		return radius*radius*Math.PI;
	}

	
	public double getPerimeter() {
		return radius*2*Math.PI;
	}

}
